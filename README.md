# Gestion Projet TP 2

# Objectifs du module
- Comprendre l'utilisation de la gestion des configuratios
- Apprendre les bases d'Ansible


- Adapter le contexte Ansible au configuration 


# Plan à suivre 

- Introduction sur la gestion des configurations
- Présentation des outils de gestions des configurations
    - Definition
    - Automatisation
    - Orchestration
- Presentation d'Ansible
    - Definition 
    - Fonctionnement
    - Les composants
- Présentation des commandes ah-doc
- Presentation des playbooks
    - Notion de variable, de boucles, de templates jinja2, filtres
    - Notion de rôles, tags, facts
- Présentation de ansible-galaxy
